# random-bytes-generate

Random bytes generated with possibilities of Numbers, Alphanumeric, Alphanumeric with special characters. 

# Installation

`npm i random-bytes-generate --save`

Then...

# Import

```
const randomBytes = require('random-bytes-generate');

or

const { generateNumber, generateAlphaNumeric } = require('random-bytes-generate');

or

import { generateNumber, generateAlphaNumeric } from 'random-bytes-generate';

```
# Random Number

```
console.log(generateNumber());  // random bytes generated, default 4 digit. sample: 4567

```

# Random Alphanumeric

```
console.log(generateAlphaNumeric());  // random bytes generated, default 4 digit. sample: 2A03

```

# Sample code

```
const { generateNumber, generateAlphaNumeric } = require('random-bytes-generate');

// generate random number bytes
console.log(generateNumber());  // random bytes generated, default 4 digit. sample: 4567

console.log(generateNumber(5));  // random bytes generated 5 digit, sample: 52208

console.log(generateNumber(4, 10));  // random bytes generated between 4 to 10 digit, sample: 978807 or 9066

console.log(generateNumber(4, 8, 2000));  // random bytes generated between 4 to 8 digit and start above 2000, sample: 2456

console.log(generateNumber(4, 8, 2000, 30000));  // random bytes generated between 4 to 8 digit and range between 2000 - 30000, sample: 2456


// generate random alphanumeric bytes
console.log(generateAlphaNumeric());  // random bytes generated, default 4 digit. sample: 2A03

console.log(generateAlphaNumeric(6));  // random bytes generated 6 digit. sample: 4Zxb02

console.log(generateAlphaNumeric(4, 8));  // random bytes generated between 4 to 8 digit. sample: JkG450

console.log(generateAlphaNumeric(4, 8, true));  // random bytes generated between 4 to 8 digit include special char (default false set) . sample: K@pE708

```

# Parameters

generateNumber(minLength, maxLength, startValue, endValue);

1. minLength - minimum length of random bytes, default value is 4
2. maxLength - maximum length of random bytes, optional.
3. startValue - start value of random bytes, optional.
4. endValue - end value of random bytes, optional.
 
generateAlphaNumeric(minLength, maxLength, specialChar);

1. minLength - minimum length of random bytes, default value is 4
2. maxLength - maximum length of random bytes, optional.
3. specialChar - true or false to include special char, optional.
 