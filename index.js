/**********************************************************************
 * Project: Random bytes generate
 * author: hemkanth <dev.hemkanth@gmail.com>
 * date: 23 November 2019.
 * Description: Random bytes generated with possibilities 
 *              number, aplha + number, alpha + number + special char.
 **********************************************************************/


/**
 * Validate param return true if value either null or number greater than 0
 * @param {*} param - any type of param value   
 */
function validateParams(param) {
    if ((param > 0 && typeof param == "number") || param == null) {
        return true;
    } else {
        return false;
    }
}

/**
 * Number Generate
 * @param {Number} minLength - minimum length of the number
 * @param {Number} maxLength - maximum length of the number (optional)
 * @param {Number} startValue - start value range between min and max length (optional)
 * @param {Number} endValue - end value range between min and max length (optional)
 */
function generateNumber(minLength = 4, maxLength = null, startValue = null, endValue = null) {

    // validate min length
    if (!validateParams(minLength) && minLength == null) return "Invalid parameter, minLength should be integer start from 1";
    
    // validate max length 
    if (!validateParams(maxLength)) return "Invalid parameter, maxLength should be integer start from minLength value";
    if (maxLength != null && (maxLength < minLength)) return "Invalid parameter, maxLength should be integer start from minLength value";
    
    // validate start and end value 
    if (!validateParams(startValue)) return "Invalid parameter, startValue should be integer and value in range of min and max length";
    if (!validateParams(endValue)) return "Invalid parameter, endValue should be integer and value in range of min and max length";

    // min length 
    const _minLength = Math.pow(10, minLength - 1);

    // max length
    const _maxLengthTemp = (maxLength == null) ? minLength : maxLength;
    const _maxLength = 90 * Math.pow(10, _maxLengthTemp - 2);

    // validate start value if not null & set start value
    if (startValue != null && (startValue < _minLength && startValue > _maxLength)) return "Invalid parameter, startValue should be integer and value in range of min and max length";
    const _startValue = (startValue == null) ? _minLength : startValue;

    // validate end value if not null & set end value
    if (endValue != null && (endValue < _minLength && endValue > _maxLength)) return "Invalid parameter, endValue should be integer and value in range of min and max length";
    const _endValue = (endValue == null) ? _maxLength : endValue;
    

    return Math.floor(_startValue + Math.random() * (_endValue - _startValue));
}


/**
 * Alpha Numeric optional include special char
 * @param {Number} minLength - minimum length of the number
 * @param {Number} maxLength - maximum length of the number (optional)
 * @param {Boolean} specialChar - to include special char or not (optional, default false)
 */
function generateAlphaNumeric(minLength = 4, maxLength = null, specialChar = false) {

    // validate min length
    if (!validateParams(minLength) && minLength == null) return "Invalid parameter, minLength should be integer start from 1";

    // validate max length 
    if (!validateParams(maxLength)) return "Invalid parameter, maxLength should be integer start from minLength value";
    if (maxLength != null && (maxLength < minLength)) return "Invalid parameter, maxLength should be integer start from minLength value";

    // validate is special char
    if (typeof specialChar != "boolean") return "Invalid parameter, specialChar should be a boolean either true or false";

    // variable store all string
    let _string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const _specialString = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+';
    
    // add specialChar if specialChar true
    if (specialChar) _string += _specialString;

    // length of the randombytes
    const _randomBytesLength = (maxLength == null) ? minLength : Math.floor(minLength + Math.random() * (maxLength - minLength));

    // variable to store random bytes
    let _randomBytes = '';

    // generate randombyte
    for (let i = 0; i < _randomBytesLength; i++) {
        _randomBytes += _string[Math.floor(Math.random()*(_string.length))];
    }

    // retrun random bytes
    return _randomBytes;

}

module.exports = {
    generateNumber,
    generateAlphaNumeric
}